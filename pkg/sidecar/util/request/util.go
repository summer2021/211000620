package request

import (
	"context"
	"fmt"
	"net/http"

	"github.com/openyurtio/openyurt/pkg/yurthub/kubernetes/serializer"
	"k8s.io/apimachinery/pkg/runtime/schema"
	"k8s.io/apiserver/pkg/endpoints/handlers/responsewriters"
	apirequest "k8s.io/apiserver/pkg/endpoints/request"
	"k8s.io/klog/v2"
)

// ReqString formats a string for request
func ReqString(req *http.Request) string {
	ctx := req.Context()
	tenant, _ := TenantInfoFrom(ctx)
	if info, ok := apirequest.RequestInfoFrom(ctx); ok {
		return fmt.Sprintf("%v %s %s: %s", tenant, info.Verb, info.Resource, req.URL.String())
	}

	return fmt.Sprintf("%s of %s", tenant, req.URL.String())
}

// TenantInfoFrom returns the value of the tenant key on the ctx
func TenantInfoFrom(ctx context.Context) (Tenant, bool) {
	info, ok := ctx.Value(tenantKey).(Tenant)
	return info, ok
}

// Err write err to response writer
func Err(err error, w http.ResponseWriter, req *http.Request) {
	ctx := req.Context()
	if info, ok := apirequest.RequestInfoFrom(ctx); ok {
		gv := schema.GroupVersion{
			Group:   info.APIGroup,
			Version: info.APIVersion,
		}
		negotiatedSerializer := serializer.YurtHubSerializer.GetNegotiatedSerializer(gv.WithResource(info.Resource))
		responsewriters.ErrorNegotiated(err, negotiatedSerializer, gv, w, req)
		return
	}

	klog.Errorf("request info is not found when err write, %s", ReqString(req))
}
