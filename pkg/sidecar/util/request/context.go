/*
Copyright 2020 The OpenYurt Authors.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package request

import (
	"context"

	"k8s.io/apiserver/pkg/authentication/user"
	genericapirequest "k8s.io/apiserver/pkg/endpoints/request"

	"sidecar/pkg/sidecar/authentication/util"
)

const tenantKey = 100

type Tenant struct {
	name       string
	authenType int
	user       user.Info
	err        error
}

func (t *Tenant) GetName() string {
	return t.name
}

func (t *Tenant) GetAuthenType() int {
	return t.authenType
}

func (t *Tenant) GetUser() user.Info {
	return t.user
}

func (t *Tenant) GetErr() error {
	return t.err
}

func GetTenantInfo(user user.Info) Tenant {
	authenName, err, authen := util.GetTenantNameFromUserInfo(user)
	return Tenant{
		name:       authenName,
		authenType: authen,
		user:       user,
		err:        err,
	}
}

// WithTenant returns a copy of parent in which the user value is set
func WithTenant(parent context.Context, tenant Tenant) context.Context {
	return genericapirequest.WithValue(parent, tenantKey, tenant)
}

// TenantFrom returns the value of the user key on the ctx
func TenantFrom(ctx context.Context) (Tenant, bool) {
	user, ok := ctx.Value(tenantKey).(Tenant)
	return user, ok
}
