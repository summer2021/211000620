/*
Copyright 2020 The OpenYurt Authors.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package server

import (
	"fmt"
	"net"
	"net/http"

	"github.com/gorilla/mux"
	"github.com/prometheus/client_golang/prometheus/promhttp"
	"k8s.io/kubernetes/cmd/kubeadm/app/cmd/options"

	"sidecar/cmd/yurt-tenant-sidecar/app/config"
)

// Server is an interface for providing http service for sidecar
type Server interface {
	Run(stopCh <-chan struct{}) error
}

// SideCarServer includes sidecarServer and proxyserver.
// sidecarServer handles requests by sidecar itself, profiling, metrics, healthz.
// proxyServer handles proxy requests to kube-apiserver.
type SideCarServer struct {
	sidecarServer     *http.Server
	proxyServer       *http.Server
	secureProxyServer *http.Server
}

// NewSideCarServer creates a Server object
func NewSideCarServer(sidecarHandler *SideCarReverseProxyHandler,
	cfg config.CompletedConfig,
) (Server, error) {
	mh := mux.NewRouter()
	registerHandlers(mh)

	s := &SideCarServer{
		sidecarServer: &http.Server{
			Addr:           net.JoinHostPort(cfg.Address, cfg.ServerPort),
			Handler:        mh,
			MaxHeaderBytes: 1 << 20,
		},
		proxyServer: &http.Server{
			Addr:    net.JoinHostPort(cfg.Address, cfg.InsurePort),
			Handler: sidecarHandler,
		},
		secureProxyServer: &http.Server{
			Addr:    net.JoinHostPort(cfg.Address, cfg.Port),
			Handler: sidecarHandler,
		},
	}

	return s, nil
}

// Run will start sidecar server and proxy server
func (s *SideCarServer) Run(stopCh <-chan struct{}) error {
	go func() {
		err := s.sidecarServer.ListenAndServe()
		if err != nil {
			panic(err)
		}
	}()

	go func() {
		err := s.secureProxyServer.ListenAndServeTLS("", "")
		if err != nil {
			panic(err)
		}
	}()

	err := s.proxyServer.ListenAndServe()
	if err != nil {
		return err
	}
	return nil
}

// registerHandler registers handlers for sideCarServer, and sideCarServer can handle requests like healthz.
func registerHandlers(c *mux.Router) {
	// register handler for health check
	c.HandleFunc("/v1/healthz", healthz).Methods("GET")

	// register handler for metrics
	c.Handle("/metrics", promhttp.Handler())
}

// healthz returns ok for healthz request
func healthz(w http.ResponseWriter, _ *http.Request) {
	w.WriteHeader(http.StatusOK)
	fmt.Fprintf(w, "OK")
}
