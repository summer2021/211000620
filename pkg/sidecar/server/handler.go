/*
Copyright 2020 The OpenYurt Authors.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package server

import (
	"net/http"
	"net/http/httputil"
	"net/url"
)

type SideCarReverseProxyHandler struct {
	reverseProxy     *httputil.ReverseProxy
	remoteServer     *url.URL
	FullHandlerChain http.Handler
}

// HandlerChainBuilderFn is used to apply filtering like authentication and tenantfilter
type HandlerChainBuilderFn func(handler http.Handler) http.Handler

// NewSideCarReverseProxyHandler creates the handler used by sidecar server
func NewSideCarReverseProxyHandler(remoteServer *url.URL, handlerChainBuilder HandlerChainBuilderFn) *SideCarReverseProxyHandler {
	proxy := httputil.NewSingleHostReverseProxy(remoteServer)
	proxy.ErrorHandler = errorHandler
	proxy.FlushInterval = -1

	return &SideCarReverseProxyHandler{
		remoteServer:     remoteServer,
		reverseProxy:     proxy,
		FullHandlerChain: handlerChainBuilder(proxy),
	}
}

func (a SideCarReverseProxyHandler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	a.FullHandlerChain.ServeHTTP(w, r)
}

func errorHandler(rw http.ResponseWriter, req *http.Request, err error) {

}
