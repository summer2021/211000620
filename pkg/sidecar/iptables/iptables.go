/*
Copyright 2020 The OpenYurt Authors.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package iptables

import (
	"k8s.io/klog/v2"
	"k8s.io/kubernetes/pkg/util/iptables"
	"k8s.io/utils/exec"
)

// EnsureIptables create iptable rules so that all requests sent to API server are forwarded to the sidecar.
func EnsureIptables(apiserverPort, sidecarPort string) error {
	protocol := iptables.ProtocolIpv4
	execer := exec.New()
	im := iptables.New(execer, protocol)

	// ensure chains for rules
	if _, err := im.EnsureChain(iptables.TableNAT, iptables.ChainPrerouting); err != nil {
		klog.Errorf("could not ensure chain for %s, %v", iptables.ChainPrerouting, err)
		return err
	}
	if _, err := im.EnsureChain(iptables.TableNAT, iptables.ChainOutput); err != nil {
		klog.Errorf("could not ensure chain for %s, %v", iptables.ChainOutput, err)
		return err
	}

	// ensure rules for sidecar
	if _, err := im.EnsureRule(iptables.Prepend, iptables.TableNAT, iptables.ChainPrerouting,
		"-p", "tcp",
		"--dport", string(apiserverPort),
		"-j", "REDIRECT",
		"--to-ports", string(sidecarPort)); err != nil {
		klog.Errorf("could not ensure -j REDIRECT iptables rule for %s:%s: %v", iptables.TableNAT, iptables.ChainPrerouting, err)
		return err
	}

	if _, err := im.EnsureRule(iptables.Prepend, iptables.TableNAT, iptables.ChainOutput,
		"-p", "tcp",
		"--dport", string(apiserverPort),
		"! --sport", string(sidecarPort),
		"-j", "REDIRECT",
		"--to-ports", string(sidecarPort)); err != nil {
		klog.Errorf("could not ensure -j REDIRECT iptables rule for %s:%s: %v", iptables.TableNAT, iptables.ChainOutput, err)
		return err
	}
	return nil
}
