/*
Copyright 2020 The OpenYurt Authors.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package authentication

import (
	"net/http"

	"k8s.io/apimachinery/pkg/api/errors"
	"k8s.io/apiserver/pkg/authentication/authenticator"
	"k8s.io/klog/v2"

	"sidecar/pkg/sidecar/util/request"
)

// WithAuthentication creates an http handler that tries to authenticate the given request as tenant which caontains
// user info, and then stores any such user found onto the provided context for the request. If authentication fails
// or returns an error we will return directly. On success, "Authorization" header is not removed from the request
// and handler is invoked to serve the request.
func WithAuthentication(handler http.Handler, auth authenticator.Request) http.Handler {
	if auth == nil {
		klog.Warningf("Authentication is disabled")
		return handler
	}
	return http.HandlerFunc(func(w http.ResponseWriter, req *http.Request) {
		resp, ok, err := auth.AuthenticateRequest(req)
		if err != nil || !ok {
			if err != nil {
				klog.Errorf("Unable to authenticate the request due to an error: %v", err)
			}
			request.Err(errors.NewBadRequest("Unable to authenticate the request due to an error."), w, req)
			return
		}

		req = req.WithContext(request.WithTenant(req.Context(), request.GetTenantInfo(resp.User)))
		handler.ServeHTTP(w, req)
	})
}
