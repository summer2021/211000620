/*
Copyright 2020 The OpenYurt Authors.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package util

import (
	"fmt"
	"io"
	"io/ioutil"
	"net/http"
	"reflect"
	"strings"

	"k8s.io/api/certificates/v1beta1"
	"k8s.io/apimachinery/pkg/api/errors"
	"k8s.io/apimachinery/pkg/runtime"
	"k8s.io/apiserver/pkg/authentication/serviceaccount"
	"k8s.io/apiserver/pkg/authentication/user"
	"k8s.io/kubernetes/pkg/auth/nodeidentifier"

	"sidecar/pkg/sidecar/tenantfilter/util"
)

// GetTenantNameFromUserInfo get tenant info from userinfo.
// Only work when authentications are bootstrap, x509, service account.
func GetTenantNameFromUserInfo(u user.Info) (string, error, int) {
	if u == nil {
		return "", fmt.Errorf("user infois nil when get tenant name"), util.ErrAuthenticator
	}

	name := u.GetName()
	groups := u.GetGroups()
	if _, isNode := nodeidentifier.NewDefaultNodeIdentifier().NodeIdentity(u); isNode {
		if isTenantName(name) {
			return strings.TrimPrefix(name, util.TENANTPREFIX), nil, util.TenantAuthenticator
		} else {
			name, err := getTenantFromGroup(groups)
			return name, err, util.NodeAuthenticator
		}
	} else if tenant, isSa := ServiceAccountIdentity(u); isSa {
		return tenant, nil, util.ServiceAccountAuthenticator
	} else if tenant, isBs := BootstrapTokenIdentity(u); isBs {
		return tenant, nil, util.BootstrapAuthenticator
	}
	return "", nil, util.ErrAuthenticator
}

func isTenantName(name string) bool {
	return strings.HasPrefix(name, util.TENANTPREFIX)
}

func getTenantFromGroup(groups []string) (string, error) {
	var t []string
	for _, g := range groups {
		if strings.HasPrefix(g, util.TENANTPREFIX) {
			names := strings.Split(g, ":")
			if len(names) == 3 {
				t = append(t, names[2])
			}
		}
	}
	l := len(t)
	if l > 1 {
		return "", fmt.Errorf("multi tenant name in group")
	} else if l == 0 {
		return "", nil
	} else {
		return t[0], nil
	}
}

// ServiceAccountIdentity get tenant info if user is authenticated through service account
func ServiceAccountIdentity(u user.Info) (string, bool) {
	if u == nil {
		return "", false
	}

	name := u.GetName()
	if !strings.HasPrefix(name, serviceaccount.ServiceAccountUsernamePrefix) {
		return "", false
	}

	isSa := false
	for _, group := range u.GetGroups() {
		if group == serviceaccount.AllServiceAccountsGroup {
			isSa = true
			break
		}
	}

	if !isSa {
		return "", false
	}

	nsName := strings.TrimPrefix(name, serviceaccount.ServiceAccountUsernamePrefix)
	nsNameSlice := strings.Split(nsName, ":")
	return nsNameSlice[0], true
}

// BootstrapTokenIdentity get tenant info if user is authenticated through bootstrap
func BootstrapTokenIdentity(u user.Info) (string, bool) {
	if u == nil {
		return "", false
	}

	name := u.GetName()
	if !strings.HasPrefix(name, "system:bootstrap") {
		return "", false
	}

	isBs := false
	for _, group := range u.GetGroups() {
		if group == "system:bootstrappers" {
			isBs = true
			break
		}
	}
	if !isBs {
		return "", false
	}

	groups := u.GetGroups()
	nsName, _ := getTenantFromGroup(groups)
	return nsName, true
}

func LimitedReadBody(req *http.Request, limit int64) ([]byte, error) {
	defer req.Body.Close()
	if limit <= 0 {
		return ioutil.ReadAll(req.Body)
	}
	lr := &io.LimitedReader{
		R: req.Body,
		N: limit + 1,
	}
	data, err := ioutil.ReadAll(lr)
	if err != nil {
		return nil, err
	}
	if lr.N <= 0 {
		return nil, errors.NewRequestEntityTooLargeError(fmt.Sprintf("limit is %d", limit))
	}
	return data, nil
}

// GetTenantFromCsrObject get tenant info from csr object.
func GetTenantFromCsrObject(obj runtime.Object) string {
	var ns string
	reqVal := reflect.ValueOf(obj)
	if reqVal.Kind() != reflect.Ptr {
		return ns
	}

	reqElem := reqVal.Elem()
	reqType := reqElem.Type()
	for i := 0; i < reqElem.NumField(); i++ {
		if reqType.Field(i).Name == "Spec" {
			specField := reqElem.Field(i)
			specObj, ok := specField.Interface().(v1beta1.CertificateSigningRequest)
			if !ok {
				return ns
			}
			specVal := reflect.ValueOf(&specObj)
			if specVal.Kind() != reflect.Ptr {
				return ns
			}

			specElem := specVal.Elem()
			specType := specElem.Type()
			for j := 0; j < specElem.NumField(); j++ {
				if specType.Field(j).Name == "Groups" {
					groupsField := specElem.Field(j)
					if groups, ok := groupsField.Interface().([]string); ok {
						ns, _ = getTenantFromGroup(groups)
					}
					break
				}
			}
		}
	}
	return ns
}
