/*
Copyright 2020 The OpenYurt Authors.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package options

import (
	filter "sidecar/pkg/sidecar/tenantfilter"
	"sidecar/pkg/sidecar/tenantfilter/admit"
	"sidecar/pkg/sidecar/tenantfilter/csrcheck"
	"sidecar/pkg/sidecar/tenantfilter/label"
	"sidecar/pkg/sidecar/tenantfilter/tenantcheck"
	"sidecar/pkg/sidecar/tenantfilter/tenantfilter"
)

// allOrderedTenantFilters is the list of all the tenant filters in order
var allOrderedTenantFilters = []string{
	tenantfilter.FilterName,
	label.FilterName,
	tenantcheck.FilterName,
	csrcheck.FilterName,
	admit.FilterName,
}

// registerAllTenantFilters registers all tenant filters and
// sets the recommended filters order. The front registered filter
// will be called before the later registered ones.
func registerAllTenantFilters(filters *filter.Filters) {
	tenantfilter.Register(filters)
	label.Register(filters)
	tenantcheck.Register(filters)
	csrcheck.Register(filters)
	admit.Register(filters)
}
