/*
Copyright 2020 The OpenYurt Authors.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package options

import (
	"fmt"

	"github.com/spf13/pflag"
	"k8s.io/apimachinery/pkg/util/sets"

	serverconfig "sidecar/cmd/yurt-tenant-sidecar/app/config"
	"sidecar/pkg/sidecar/tenantfilter"
	"sidecar/pkg/sidecar/tenantfilter/initializer"
)

// TenantFilterOptions contains all tenant filter options for sidecar
type TenantFilterOptions struct {
	// RecommendedFilterOrder holds an ordered list of filter names we recommend to use by default
	RecommendedFilterOrder []string
	// DisableFilters indicates filters to be disabled passed through `--disable-tenant-filters`
	DisableTenantFilters []string
	// Filters contains all registered filters
	Filters *tenantfilter.Filters
}

// NewTenantFilterOptions create a new TenantFilterOptions and register all tenant filters.
func NewTenantFilterOptions() *TenantFilterOptions {
	o := &TenantFilterOptions{
		Filters:                tenantfilter.NewFilters(),
		RecommendedFilterOrder: allOrderedTenantFilters,
	}
	// register all tenant filters
	registerAllTenantFilters(o.Filters)
	return o

}

// AddFlags returns flags of tenant filters for a sidecar
func (o *TenantFilterOptions) AddFlags(fs *pflag.FlagSet) {
	if o == nil {
		return
	}
	fs.StringSliceVar(&o.DisableTenantFilters, "--disable-tenant-filters", o.DisableTenantFilters, "tenant filters that should be disabled although they are enabled in defalut")
}

// Validate verifies flags passed to TenantFilterOptions.
func (f *TenantFilterOptions) Validate() []error {
	var errs []error

	registeredFilters := sets.NewString(f.Filters.Registerd()...)
	for _, name := range f.DisableTenantFilters {
		if !registeredFilters.Has(name) {
			errs = append(errs, fmt.Errorf("disable-tenant-filters filter %s is unknown", name))
		}
	}
	// Verify RecommendedFilterOrder.
	recommendFilters := sets.NewString(f.RecommendedFilterOrder...)
	intersections := registeredFilters.Intersection(recommendFilters)
	if !intersections.Equal(recommendFilters) {
		// Developer error, this should never run in.
		errs = append(errs, fmt.Errorf("filters %v in RecommendedFilterOrder are not registered",
			recommendFilters.Difference(intersections).List()))
	}
	if !intersections.Equal(registeredFilters) {
		// Developer error, this should never run in.
		errs = append(errs, fmt.Errorf("filters %v registered are not in RecommendedFilterOrder",
			registeredFilters.Difference(intersections).List()))
	}
	return errs
}

// ApplyTo fills up sidecar config with options.
func (f *TenantFilterOptions) ApplyTo(c *serverconfig.Config) error {
	if f == nil {
		return nil
	}

	genericInitializer := initializer.New(c.Client, c.SharedInformer, c.SerializerManager)
	initializerChain := tenantfilter.FilterInitializers{}
	initializerChain = append(initializerChain, genericInitializer)

	filterNames := f.enabledFilterNames()
	filterChain, err := f.Filters.NewFromFilters(filterNames, initializerChain)
	if err != nil {
		return err
	}
	c.Filters = filterChain
	return nil
}

func (f *TenantFilterOptions) enabledFilterNames() []string {
	orderedFilters := []string{}
	disableFilters := sets.NewString(f.DisableTenantFilters...)
	for _, filter := range f.RecommendedFilterOrder {
		if !disableFilters.Has(filter) {
			orderedFilters = append(orderedFilters, filter)
		}
	}
	return orderedFilters
}
