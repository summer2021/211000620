/*
Copyright 2018 The Kubernetes Authors.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package options

import (
	"fmt"
	"time"

	"github.com/spf13/pflag"

	serverconfig "sidecar/cmd/yurt-tenant-sidecar/app/config"
)

// GenericSideCarOptions contains all generic options for sidecar
type GenericSideCarOptions struct {
	Address    string
	Port       string
	InsurePort string
	ServerPort string

	MaxRequestsInFlight         int
	MaxMutatingRequestsInFlight int
	RequestTimeout              time.Duration
	LivezGracePeriod            time.Duration
	MinRequestTimeout           int
	ShutdownDelayDuration       time.Duration
	// The limit on the request body size that would be accepted and
	// decoded in a write request. 0 means no limit.
	// We intentionally did not add a flag for this option. Users of the
	// apiserver library can wire it to a flag.
	MaxRequestBodyBytes int64
}

// NewDefaultGenericSideCarOptions create a default GenericSideCarOptions.
func NewDefaultGenericSideCarOptions() *GenericSideCarOptions {
	defaults := serverconfig.NewConfig()
	return &GenericSideCarOptions{
		Address:    "127.0.0.1",
		Port:       "10061",
		InsurePort: "10060",
		ServerPort: "10062",

		MaxRequestsInFlight:         defaults.MaxRequestsInFlight,
		MaxMutatingRequestsInFlight: defaults.MaxMutatingRequestsInFlight,
		RequestTimeout:              defaults.RequestTimeout,
		LivezGracePeriod:            defaults.LivezGracePeriod,
		MinRequestTimeout:           defaults.MinRequestTimeout,
		ShutdownDelayDuration:       defaults.ShutdownDelayDuration,
		MaxRequestBodyBytes:         defaults.MaxRequestBodyBytes,
	}
}

// AddFlags returns flags of authentication for a sidecar
func (o *GenericSideCarOptions) AddFlags(fs *pflag.FlagSet) {
	if o == nil {
		return
	}
	fs.StringVar(&o.InsurePort, "proxy-port", o.InsurePort, "the port on which to proxy HTTP requests to kube-apiserver")
	fs.StringVar(&o.Port, "proxy-secure-port", o.Port, "the port on which to proxy HTTPS requests to kube-apiserver")
	fs.StringVar(&o.ServerPort, "serve-port", o.ServerPort, "the port on which to serve HTTP requests(like profiling, metrics) for sidecar.")

	fs.IntVar(&o.MaxRequestsInFlight, "max-requests-inflight", o.MaxRequestsInFlight, ""+
		"This and --max-mutating-requests-inflight are summed to determine the server'o total concurrency limit "+
		"(which must be positive) if --enable-priority-and-fairness is true. "+
		"Otherwise, this flag limits the maximum number of non-mutating requests in flight, "+
		"or a zero value disables the limit completely.")

	fs.IntVar(&o.MaxMutatingRequestsInFlight, "max-mutating-requests-inflight", o.MaxMutatingRequestsInFlight, ""+
		"This and --max-requests-inflight are summed to determine the server'o total concurrency limit "+
		"(which must be positive) if --enable-priority-and-fairness is true. "+
		"Otherwise, this flag limits the maximum number of mutating requests in flight, "+
		"or a zero value disables the limit completely.")

	fs.DurationVar(&o.RequestTimeout, "request-timeout", o.RequestTimeout, ""+
		"An optional field indicating the duration a handler must keep a request open before timing "+
		"it out. This is the default request timeout for requests but may be overridden by flags such as "+
		"--min-request-timeout for specific types of requests.")

	fs.DurationVar(&o.LivezGracePeriod, "livez-grace-period", o.LivezGracePeriod, ""+
		"This option represents the maximum amount of time it should take for apiserver to complete its startup sequence "+
		"and become live. From apiserver'o start time to when this amount of time has elapsed, /livez will assume "+
		"that unfinished post-start hooks will complete successfully and therefore return true.")

	fs.IntVar(&o.MinRequestTimeout, "min-request-timeout", o.MinRequestTimeout, ""+
		"An optional field indicating the minimum number of seconds a handler must keep "+
		"a request open before timing it out. Currently only honored by the watch request "+
		"handler, which picks a randomized value above this number as the connection timeout, "+
		"to spread out load.")

	fs.DurationVar(&o.ShutdownDelayDuration, "shutdown-delay-duration", o.ShutdownDelayDuration, ""+
		"Time to delay the termination. During that time the server keeps serving requests normally. The endpoints /healthz and /livez "+
		"will return success, but /readyz immediately returns failure. Graceful termination starts after this delay "+
		"has elapsed. This can be used to allow load balancer to stop sending traffic to this server.")

}

// Validate checks invalid config combination
func (o *GenericSideCarOptions) Validate() []error {
	var errs []error

	if o.LivezGracePeriod < 0 {
		errs = append(errs, fmt.Errorf("--livez-grace-period can not be a negative value"))
	}

	if o.MaxRequestsInFlight < 0 {
		errs = append(errs, fmt.Errorf("--max-requests-inflight can not be negative value"))
	}
	if o.MaxMutatingRequestsInFlight < 0 {
		errs = append(errs, fmt.Errorf("--max-mutating-requests-inflight can not be negative value"))
	}

	if o.RequestTimeout.Nanoseconds() < 0 {
		errs = append(errs, fmt.Errorf("--request-timeout can not be negative value"))
	}

	if o.MinRequestTimeout < 0 {
		errs = append(errs, fmt.Errorf("--min-request-timeout can not be negative value"))
	}

	if o.ShutdownDelayDuration < 0 {
		errs = append(errs, fmt.Errorf("--shutdown-delay-duration can not be negative value"))
	}

	if o.MaxRequestBodyBytes < 0 {
		errs = append(errs, fmt.Errorf("--max-resource-write-bytes can not be negative value"))
	}
	return errs
}

// ApplyTo fills up sidecar config with options.
func (o *GenericSideCarOptions) ApplyTo(c *serverconfig.Config) error {
	c.Address = o.Address
	c.Port = o.Port
	c.InsurePort = o.InsurePort
	c.ServerPort = o.ServerPort

	c.MaxRequestsInFlight = o.MaxRequestsInFlight
	c.MaxMutatingRequestsInFlight = o.MaxMutatingRequestsInFlight
	c.LivezGracePeriod = o.LivezGracePeriod
	c.RequestTimeout = o.RequestTimeout
	c.MinRequestTimeout = o.MinRequestTimeout
	c.ShutdownDelayDuration = o.ShutdownDelayDuration
	c.MaxRequestBodyBytes = o.MaxRequestBodyBytes

	return nil
}
