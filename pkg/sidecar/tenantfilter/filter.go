/*
Copyright 2021 The OpenYurt Authors.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package tenantfilter

import (
	"fmt"
	"k8s.io/klog/v2"
	"sort"
	"sync"
)

// Factory is a func that returns an Interface
type Factory func() (Interface, error)

// Filters holds the tenant filter registered.
type Filters struct {
	lock     sync.Mutex
	registry map[string]Factory
}

// NewFilters creates a new Filters
func NewFilters() *Filters {
	return &Filters{
		registry: make(map[string]Factory),
	}
}

//Registerd enumerates the names of all registered filters.
func (fs *Filters) Registerd() []string {
	fs.lock.Lock()
	defer fs.lock.Unlock()

	keys := []string{}
	for k := range fs.registry {
		keys = append(keys, k)
	}
	sort.Strings(keys)
	return keys
}

// Register registers a filter Factory by name. This
// is expected to happen during app startup.
func (fs *Filters) Register(name string, filter Factory) {
	fs.lock.Lock()
	defer fs.lock.Unlock()

	_, found := fs.registry[name]
	if found {
		klog.Warningf("Filter %q has already registered", name)
		return
	}
	klog.V(2).Infof("Filter %s registered successfully", name)
	fs.registry[name] = filter
}

// NewFromFilters initializes filters by filterinitializers according filter name
func (fs *Filters) NewFromFilters(filterNames []string, filterInitializer genericFilterInitializer) (Interface, error) {
	filters := []Interface{}
	for _, name := range filterNames {
		f, found := fs.registry[name]
		if !found {
			return nil, fmt.Errorf("Filter %s has not registered", name)
		}

		ins, err := f()
		if err != nil {
			klog.Errorf("new filter %s failed, %v", name, err)
			return nil, err
		}

		filterInitializer.Initialize(ins)
		klog.V(2).Infof("Filter %s initialize successfully", name)

		filters = append(filters, ins)
	}
	if len(filters) == 0 {
		return nil, nil
	}
	return filterChain(filters), nil
}

type FilterInitializers []genericFilterInitializer

// Initialize initialize a filter
func (ff FilterInitializers) Initialize(filter Interface) {
	for _, f := range ff {
		f.Initialize(filter)
	}
}
