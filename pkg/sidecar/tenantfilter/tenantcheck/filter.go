/*
Copyright 2021 The OpenYurt Authors.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package tenantcheck

import (
	"context"
	"fmt"
	"net/http"
	"strings"

	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/util/sets"
	kuberequest "k8s.io/apiserver/pkg/endpoints/request"
	"k8s.io/client-go/kubernetes"
	clientgoclientset "k8s.io/client-go/kubernetes"
	"k8s.io/klog/v2"

	"sidecar/pkg/sidecar/tenantfilter"
	"sidecar/pkg/sidecar/tenantfilter/util"
	"sidecar/pkg/sidecar/util/request"
)

// FilterName indicates name of sidecar filter
const FilterName = "TenantCheck"

// TenantCheck check whether the tenant info obtained
// in authentication is the same as it obtained in the request.
type TenantCheck struct {
	*tenantfilter.Approver
	client kubernetes.Interface
}

// NewTenantCheck creates a TenantCheck filter.
func NewTenantCheck() *TenantCheck {
	return &TenantCheck{
		Approver: &tenantfilter.Approver{
			ClusterOperations: map[string]sets.String{
				"node":      sets.NewString([]string{util.GET, util.DELETE, util.UPDATE, util.PATCH}...),
				"namespace": sets.NewString([]string{util.GET}...),
			},
			NamespaceOperations: map[string]sets.String{
				"*": sets.NewString([]string{"*"}...),
			},
		},
	}
}

// Register registers a filter
func Register(filters *tenantfilter.Filters) {
	filters.Register(FilterName, func() (tenantfilter.Interface, error) {
		return NewTenantCheck(), nil
	})
}

func (t *TenantCheck) SetKubeClient(client kubernetes.Interface) {
	t.client = client
}

func (t *TenantCheck) Approve(req *http.Request) bool {
	ctx := req.Context()
	reqInfo, ok := kuberequest.RequestInfoFrom(ctx)
	if !ok {
		klog.V(3).Infof("nou found requestInfo in filter %s", FilterName)
		return false
	}
	tenant, ok := request.TenantFrom(ctx)
	if !ok {
		klog.V(3).Infof("not found user in filter %s", FilterName)
		return false
	}
	authenName := tenant.GetName()
	authen := tenant.GetAuthenType()
	user := tenant.GetUser()
	groups := user.GetGroups()
	name := user.GetName()
	if authen != util.TenantAuthenticator {
		//glog.V(3).Infof("authenticator is not TenantAuthenticator, skip filter")
		return false
	}
	if authenName == "" {
		klog.V(3).Infof("tenant filter: %s %s: without auth namespace or err: %v from groups/name(%s/%s)",
			reqInfo.Verb, req.URL.String(), tenant.GetErr(), strings.Join(groups, ","), name)
		return false
	}

	if !reqInfo.IsResourceRequest {
		return false
	}
	if IsPublicNamespace(reqInfo.Namespace) {
		return false
	}
	if !tenantfilter.IsClusterResource(reqInfo.Resource) && reqInfo.Namespace == "" {
		return false
	}
	return t.Approver.Approve(reqInfo.Resource, reqInfo.Verb)

}

func IsPublicNamespace(ns string) bool {
	if ns == util.KUBESYSTEMNAMESPACE || ns == util.KUBEPUBLICNAMESPACE || ns == util.KUBENODELEASENAMESPACE || ns == util.DEFAULTNAMESPACE {
		return true
	}
	return false
}

func (t *TenantCheck) Filter(req *http.Request) (err error) {
	var tenantName string
	ctx := req.Context()
	reqInfo, ok := kuberequest.RequestInfoFrom(ctx)
	if !ok {
		err = fmt.Errorf("nou found requestInfo in filter %s", FilterName)
		return
	}
	user, ok := kuberequest.UserFrom(ctx)
	if !ok {
		err = fmt.Errorf("not found user in filter %s", FilterName)
		return
	}
	groups := user.GetGroups()
	name := user.GetName()

	if reqInfo.Namespace != "" {
		tenantName = reqInfo.Namespace
	} else if reqInfo.Resource == "namespace" {
		tenantName = reqInfo.Name
	} else {
		tenantName, err = getTenantNameFromObject(t.client, reqInfo.Resource, reqInfo.Name)
		if err != nil {
			err = fmt.Errorf("tenant filter:  %s %s: get namespace from groups/name(%s/%s) failed: %v",
				reqInfo.Verb, req.URL.String(), strings.Join(groups, ","), name, err)
		}
		return
	}

	tenant, _ := request.TenantFrom(ctx)
	authenName := tenant.GetName()
	if authenName != tenantName {
		err = fmt.Errorf("tenant filter: %s %s: tenantName(%s) from url is not match authentic tenantName(%s) for groups/name(%s/%s)",
			reqInfo.Verb, req.URL.String(), tenantName, authenName, strings.Join(groups, ","), name)
	}
	return
}

func getTenantNameFromObject(client clientgoclientset.Interface, resource, name string) (string, error) {
	var ns string
	if name == "" {
		return ns, fmt.Errorf("tenant filter: name of resource(%s) is empty.", resource)
	}

	switch resource {
	//case "persistentvolumes":
	//	pv, err := client.CoreV1().PersistentVolumes().Get(name, metav1.GetOptions{})
	//	if err != nil {
	//		return "", fmt.Errorf("tenant filter: get pv(%s) failed. %v", name, err)
	//	}
	//	if pv != nil && pv.Labels != nil && pv.Labels[TenantLabelKey] != "" {
	//		ns = pv.Labels[TenantLabelKey]
	//	}
	//case "storageclasses":
	//	sc, err := client.Storage().StorageClasses().Get(name, metav1.GetOptions{})
	//	if err != nil {
	//		return "", fmt.Errorf("tenant filter: get storageclass(%s) failed. %v", name, err)
	//	}
	//	if sc != nil && sc.Labels != nil && sc.Labels[TenantLabelKey] != "" {
	//		ns = sc.Labels[TenantLabelKey]
	//	}
	//case "volumeattachments":
	//	va, err := client.Storage().VolumeAttachments().Get(name, metav1.GetOptions{})
	//	if err != nil {
	//		return "", fmt.Errorf("tenant filter: get volumeattachment(%s) failed. %v", name, err)
	//	}
	//	if va != nil && va.Labels != nil && va.Labels[TenantLabelKey] != "" {
	//		ns = va.Labels[TenantLabelKey]
	//	}
	//case "clusterroles":
	//	role, err := client.Rbac().ClusterRoles().Get(name, metav1.GetOptions{})
	//	if err != nil {
	//		return "", fmt.Errorf("tenant filter: get clusterrole(%s) failed. %v", name, err)
	//	}
	//	if role != nil && role.Labels != nil && role.Labels[TenantLabelKey] != "" {
	//		ns = role.Labels[TenantLabelKey]
	//	}
	//case "clusterrolebindings":
	//	binding, err := client.Rbac().ClusterRoleBindings().Get(name, metav1.GetOptions{})
	//	if err != nil {
	//		return "", fmt.Errorf("tenant filter: get clusterrolebinding(%s) failed. %v", name, err)
	//	}
	//	if binding != nil && binding.Labels != nil && binding.Labels[TenantLabelKey] != "" {
	//		ns = binding.Labels[TenantLabelKey]
	//	}
	case "nodes":
		node, err := client.CoreV1().Nodes().Get(context.TODO(), name, metav1.GetOptions{})
		if err != nil {
			return "", fmt.Errorf("tenant filter: get node(%s) failed. %v", name, err)
		}
		if node != nil && node.Labels != nil && node.Labels[util.TenantLabelKey] != "" {
			ns = node.Labels[util.TenantLabelKey]
		}
	default:
		return "", fmt.Errorf("tenant filter: get tenant name for resource(%s: %s) is not supported in cluster resource get request.", resource, name)
	}

	return ns, nil
}
