/*
Copyright 2020 The OpenYurt Authors.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package tenantfilter

import (
	"net/http"
)

// Interface is a method shared by filters.
// Approve is used to determine whether the filter can handle the request.
// filter is used to process the request.
type Interface interface {
	Approve(req *http.Request) bool
	Filter(req *http.Request) error
}

// genericFilterInitializer is used for initialization of shareable resources between filters.
// After initialization the resources have to be set separately
type genericFilterInitializer interface {
	Initialize(filter Interface)
}
