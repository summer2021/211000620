/*
Copyright 2021 The OpenYurt Authors.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package tenantfilter

import (
	"fmt"
	"k8s.io/klog"
	"net/http"
	"strings"

	"k8s.io/apimachinery/pkg/util/sets"
	kuberequest "k8s.io/apiserver/pkg/endpoints/request"

	"sidecar/pkg/sidecar/tenantfilter"
	"sidecar/pkg/sidecar/tenantfilter/util"
	"sidecar/pkg/sidecar/util/request"
)

const (
	FilterName = "TenantFilter"
)

// TenantFilter add the tenant label for the request.
type TenantFilter struct {
	*tenantfilter.Approver
}

// NewTenantFilter creates a TenantFilter filter.
func NewTenantFilter() *TenantFilter {
	return &TenantFilter{
		Approver: &tenantfilter.Approver{
			ClusterOperations: map[string]sets.String{
				"nodes":      sets.NewString([]string{util.LIST, util.WATCH, util.DELETECOLLECTION}...),
				"namespaces": sets.NewString([]string{util.LIST, util.WATCH}...),
			},
			NamespaceOperations: map[string]sets.String{
				"*": sets.NewString([]string{util.LIST, util.WATCH, util.DELETECOLLECTION}...),
			},
		},
	}
}

// Register registers a filter
func Register(filters *tenantfilter.Filters) {
	filters.Register(FilterName, func() (tenantfilter.Interface, error) {
		return NewTenantFilter(), nil
	})
}

func (t *TenantFilter) Approve(req *http.Request) bool {
	ctx := req.Context()
	reqInfo, ok := kuberequest.RequestInfoFrom(ctx)
	if !ok {
		klog.V(3).Infof("nou found requestInfo in filter %s", FilterName)
		return false
	}
	tenant, ok := request.TenantFrom(ctx)
	if !ok {
		klog.V(3).Infof("not found user in filter %s", FilterName)
		return false
	}
	authenName := tenant.GetName()
	authen := tenant.GetAuthenType()
	user := tenant.GetUser()
	groups := user.GetGroups()
	name := user.GetName()
	if authen != util.TenantAuthenticator {
		//glog.V(3).Infof("authenticator is not TenantAuthenticator, skip filter")
		return false
	}
	if authenName == "" {
		klog.V(3).Infof("tenant filter: %s %s: without auth namespace or err: %v from groups/name(%s/%s)",
			reqInfo.Verb, req.URL.String(), tenant.GetErr(), strings.Join(groups, ","), name)
		return false
	}

	if !reqInfo.IsResourceRequest {
		return false
	}
	if reqInfo.Namespace != "" {
		return false
	}
	return t.Approver.Approve(reqInfo.Resource, reqInfo.Verb)
}

func (t *TenantFilter) Filter(req *http.Request) (err error) {
	var tenantName string
	ctx := req.Context()
	reqInfo, ok := kuberequest.RequestInfoFrom(ctx)
	if !ok {
		err = fmt.Errorf("nou found requestInfo in filter %s", FilterName)
		return
	}
	user, ok := kuberequest.UserFrom(ctx)
	if !ok {
		err = fmt.Errorf("not found user in filter %s", FilterName)
		return
	}
	groups := user.GetGroups()
	name := user.GetName()
	tenant, _ := request.TenantFrom(ctx)
	tenantName = tenant.GetName()

	values := req.URL.Query()
	ls, ok := values["labelSelector"]
	if !ok {
		values.Set("labelSelector", fmt.Sprintf("%s=%s", util.TenantLabelKey, tenantName))
	} else {
		for _, l := range ls {
			arr := strings.Split(l, "=")
			if arr != nil && arr[0] == util.TenantLabelKey {
				if len(arr) != 2 || arr[1] != tenantName {
					err = fmt.Errorf("tenant filter: %s %s: tenantName(%s) from labelselector is not match authentic tenantName(%s) for groups/name(%s/%s)",
						reqInfo.Verb, req.URL.String(), arr[1], tenantName, strings.Join(groups, ","), name)
					return
				}
				return
			}
		}
		values.Add("labelSelector", fmt.Sprintf("%s=%s", util.TenantLabelKey, tenantName))
	}
	req.URL.RawQuery = values.Encode()
	klog.V(2).Infof("tenant filter: add labelSelector to query(%s %s) from groups/name(%s/%s)",
		reqInfo.Verb, req.URL.String(), strings.Join(groups, ","), name)
	return
}
