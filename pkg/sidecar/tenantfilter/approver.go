/*
Copyright 2021 The OpenYurt Authors.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package tenantfilter

import "k8s.io/apimachinery/pkg/util/sets"

type Approver struct {
	ClusterOperations   map[string]sets.String
	NamespaceOperations map[string]sets.String
}

func NewApprover() Approver {
	return Approver{
		ClusterOperations:   make(map[string]sets.String),
		NamespaceOperations: make(map[string]sets.String),
	}
}

func (a *Approver) Approve(resouce, verb string) bool {
	var verbs sets.String
	var ok bool
	if IsClusterResource(resouce) {
		_, ok = a.ClusterOperations["*"]
		if ok {
			return true
		}
		verbs, ok = a.ClusterOperations[resouce]
	} else {
		_, ok = a.NamespaceOperations["*"]
		if ok {
			return true
		}
		verbs, ok = a.NamespaceOperations[resouce]
	}
	if !ok {
		return false
	}
	if verbs.Has("*") {
		return true
	}
	return verbs.Has(verb)
}

func IsClusterResource(resource string) bool {
	return true
}
