/*
Copyright 2021 The OpenYurt Authors.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package csrcheck

import (
	"fmt"
	"k8s.io/klog/v2"
	"net/http"
	"strings"

	"github.com/openyurtio/openyurt/pkg/yurthub/kubernetes/serializer"
	"k8s.io/apimachinery/pkg/util/sets"
	kuberequest "k8s.io/apiserver/pkg/endpoints/request"

	"sidecar/pkg/sidecar/authentication/util"
	"sidecar/pkg/sidecar/tenantfilter"
	filterutil "sidecar/pkg/sidecar/tenantfilter/util"
	"sidecar/pkg/sidecar/util/request"
)

// FilterName indicates name of sidecar filter
const FilterName = "CsrCheck"

// CsrCheck check whether the tenant info obtained
// in authentication is the same as it obtained in the csr obj.
type CsrCheck struct {
	*tenantfilter.Approver
	serializerManager   *serializer.SerializerManager
	MaxRequestBodyBytes int64
}

// NewTenantCheck creates a CsrCheck filter.
func NewCsrCheck() *CsrCheck {
	return &CsrCheck{
		Approver: &tenantfilter.Approver{
			ClusterOperations: map[string]sets.String{
				"certificateSigningRequest": sets.NewString([]string{filterutil.CREATE}...),
			},
		},
	}
}

// Register registers a filter
func Register(filters *tenantfilter.Filters) {
	filters.Register(FilterName, func() (tenantfilter.Interface, error) {
		return NewCsrCheck(), nil
	})
}

func (t *CsrCheck) SetSerializerManager(s *serializer.SerializerManager) {
	t.serializerManager = s
}

func (t *CsrCheck) Approve(req *http.Request) bool {
	ctx := req.Context()
	reqInfo, ok := kuberequest.RequestInfoFrom(ctx)
	if !ok {
		klog.V(3).Infof("nou found requestInfo in filter %s", FilterName)
		return false
	}
	tenant, ok := request.TenantFrom(ctx)
	if !ok {
		klog.V(3).Infof("not found user in filter %s", FilterName)
		return false
	}
	authenName := tenant.GetName()
	authen := tenant.GetAuthenType()
	user := tenant.GetUser()
	groups := user.GetGroups()
	name := user.GetName()
	if authen != filterutil.BootstrapAuthenticator {
		//glog.V(3).Infof("authenticator is not TenantAuthenticator, skip filter")
		return false
	}
	if authenName == "" {
		klog.V(3).Infof("tenant filter: %s %s: without auth namespace or err: %v from groups/name(%s/%s)",
			reqInfo.Verb, req.URL.String(), tenant.GetErr(), strings.Join(groups, ","), name)
		return false
	}

	if !reqInfo.IsResourceRequest {
		return false
	}
	return t.Approver.Approve(reqInfo.Resource, reqInfo.Verb)
}

func (t *CsrCheck) Filter(req *http.Request) (err error) {
	var tenantName string
	ctx := req.Context()
	reqInfo, ok := kuberequest.RequestInfoFrom(ctx)
	if !ok {
		err = fmt.Errorf("nou found requestInfo in filter %s", FilterName)
		return
	}
	user, ok := kuberequest.UserFrom(ctx)
	if !ok {
		err = fmt.Errorf("not found user in filter %s", FilterName)
		return
	}
	groups := user.GetGroups()
	name := user.GetName()

	s := filterutil.CreateSerializer(req, t.serializerManager)
	if s == nil {
		return fmt.Errorf("failed to create serializer in TenantLabel Filter")
	}
	body, err := util.LimitedReadBody(req, t.MaxRequestBodyBytes)
	if err != nil {

	}
	obj, err := s.Decode(body)
	if err != nil || obj == nil {
		return fmt.Errorf("failed to decode request in TenantLabel Filter, %v", err)
	}
	if tenantName == "" {
		tenantName = util.GetTenantFromCsrObject(obj)
	}

	tenant, _ := request.TenantFrom(ctx)
	authenName := tenant.GetName()
	if authenName != tenantName {
		err = fmt.Errorf("tenant filter: %s %s: tenantName(%s) from url is not match authentic tenantName(%s) for groups/name(%s/%s)",
			reqInfo.Verb, req.URL.String(), tenantName, authenName, strings.Join(groups, ","), name)
	}
	return
}
