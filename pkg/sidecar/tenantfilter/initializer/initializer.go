/*
Copyright 2020 The OpenYurt Authors.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package initializer

import (
	"github.com/openyurtio/openyurt/pkg/yurthub/kubernetes/serializer"
	"k8s.io/client-go/informers"
	"k8s.io/client-go/kubernetes"

	"sidecar/pkg/sidecar/tenantfilter"
)

// genericFilterInitializer holds generic config for tenant filters.
type genericFilterInitializer struct {
	client            kubernetes.Interface
	informers         informers.SharedInformerFactory
	serializerManager *serializer.SerializerManager
}

// New creates a New genericFilterInitializer
func New(
	cli kubernetes.Interface,
	in informers.SharedInformerFactory,
	sm *serializer.SerializerManager,
) *genericFilterInitializer {
	return &genericFilterInitializer{
		client:            cli,
		informers:         in,
		serializerManager: sm,
	}
}

// Initialize initialize the filter.
func (i *genericFilterInitializer) Initialize(filter tenantfilter.Interface) {
	if wants, ok := filter.(WantsClient); ok {
		wants.SetKubeClient(i.client)
	}
	if wants, ok := filter.(WantsSharedInformer); ok {
		wants.SetSharedInformer(i.informers)
	}
	if wants, ok := filter.(WantsSerializerManager); ok {
		wants.SetSerializerManager(i.serializerManager)
	}
}

// WantsClient is an interface for setting kubeclient
type WantsClient interface {
	SetKubeClient(client kubernetes.Interface)
}

// WantsSharedInformer is an interface for setting SharedInformerFactory
type WantsSharedInformer interface {
	SetSharedInformer(informer informers.SharedInformerFactory)
}

// WantsStorageWrapper is an interface for setting StorageWrapper
type WantsSerializerManager interface {
	SetSerializerManager(s *serializer.SerializerManager)
}
