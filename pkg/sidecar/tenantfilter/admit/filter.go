/*
Copyright 2021 The OpenYurt Authors.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package admit

import (
	"net/http"

	"k8s.io/apiserver/pkg/endpoints/request"
	"k8s.io/klog/v2"

	authutil "sidecar/pkg/sidecar/authentication/util"
	"sidecar/pkg/sidecar/tenantfilter"
	"sidecar/pkg/sidecar/tenantfilter/util"
)

// FilterName indicates name of sidecar filter
const FilterName = "Admit"

// Admit return true.
type Admit struct{}

// NewAdmit creates a Admit filter.
func NewAdmit() *Admit {
	return &Admit{}
}

// Register registers a filter
func Register(filters *tenantfilter.Filters) {
	filters.Register(FilterName, func() (tenantfilter.Interface, error) {
		return NewAdmit(), nil
	})
}

func (t *Admit) Approve(req *http.Request) bool {
	ctx := req.Context()
	reqInfo, ok := request.RequestInfoFrom(ctx)
	if !ok {
		klog.V(3).Infof("nou found requestInfo in filter %s", FilterName)
		return false
	}
	user, ok := request.UserFrom(ctx)
	if !ok {
		klog.V(3).Infof("not found user in filter %s", FilterName)
		return false
	}
	_, _, authen := authutil.GetTenantNameFromUserInfo(user)
	if authen == util.NodeAuthenticator { // 1/2/4)
		return true
	} else if authen == util.BootstrapAuthenticator && !isCsrRequest(reqInfo.Resource, reqInfo.Verb) { // 7)
		return true
	} else if authen == util.TenantAuthenticator && !reqInfo.IsResourceRequest { // 5)
		return true
	} else if authen == util.ServiceAccountAuthenticator { // 3\6)
		// 待确认,包含用户 pod 以及公共组件 pod 两种情况
		return true
	}
	return false
}

func (t *Admit) Filter(req *http.Request) (err error) {
	return nil
}

func isCsrRequest(resourse, verb string) bool {
	if resourse == "certificateSigningRequest" && verb == util.CREATE {
		return true
	}
	return false
}
