/*
Copyright 2020 The OpenYurt Authors.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package tenantfilter

import (
	"fmt"
	"net/http"

	"k8s.io/apimachinery/pkg/api/errors"
	"k8s.io/klog/v2"

	util "sidecar/pkg/sidecar/util/request"
)

// WithTenantFilter creates an http handler that tries to filter the given request according to the tenant info
// found onto the provided context for the request. If tenant filter fails or returns an error we will return directly.
func WithTenantFilter(handler http.Handler, filterChain Interface) http.Handler {
	if filterChain == nil {
		klog.Warning("filter is disabled")
		return handler
	}
	return http.HandlerFunc(func(w http.ResponseWriter, req *http.Request) {
		err := filterChain.Filter(req)
		if err != nil {
			klog.Errorf("failed to filter request for %s, %v", util.ReqString(req), err)
			util.Err(errors.NewBadRequest(fmt.Sprintf("Unable to filter the request due to an error: %v", err)), w, req)
			return

		}
		handler.ServeHTTP(w, req)
	})
}
