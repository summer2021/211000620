/*
Copyright 2021 The OpenYurt Authors.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package label

import (
	"fmt"
	"io"
	"io/ioutil"
	"net/http"
	"reflect"
	"strings"

	"github.com/openyurtio/openyurt/pkg/yurthub/kubernetes/serializer"
	"k8s.io/apimachinery/pkg/api/errors"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/util/sets"
	kuberequest "k8s.io/apiserver/pkg/endpoints/request"
	"k8s.io/klog/v2"

	"sidecar/pkg/sidecar/tenantfilter"
	"sidecar/pkg/sidecar/tenantfilter/util"
	filterutil "sidecar/pkg/sidecar/tenantfilter/util"
	"sidecar/pkg/sidecar/util/request"
)

const FilterName = "TenantLable"

// TenantLabel add the tenant label for the resource in the request.
type TenantLabel struct {
	*tenantfilter.Approver
	serializerManager   *serializer.SerializerManager
	MaxRequestBodyBytes int64
}

// NewTenantLabel creates a CsrCheck filter.
func NewTenantLabel() *TenantLabel {
	return &TenantLabel{
		Approver: &tenantfilter.Approver{
			ClusterOperations: map[string]sets.String{
				"node": sets.NewString([]string{util.CREATE}...),
			},
		},
	}
}

// Register registers a filter
func Register(filters *tenantfilter.Filters) {
	filters.Register(FilterName, func() (tenantfilter.Interface, error) {
		return NewTenantLabel(), nil
	})
}

func (t *TenantLabel) SetSerializerManager(s *serializer.SerializerManager) {
	t.serializerManager = s
}

func (t *TenantLabel) Approve(req *http.Request) bool {
	ctx := req.Context()
	reqInfo, ok := kuberequest.RequestInfoFrom(ctx)
	if !ok {
		klog.V(3).Infof("nou found requestInfo in filter %s", FilterName)
		return false
	}
	tenant, ok := request.TenantFrom(ctx)
	if !ok {
		klog.V(3).Infof("not found user in filter %s", FilterName)
		return false
	}
	authenName := tenant.GetName()
	authen := tenant.GetAuthenType()
	user := tenant.GetUser()
	groups := user.GetGroups()
	name := user.GetName()
	if authen != util.TenantAuthenticator {
		//glog.V(3).Infof("authenticator is not TenantAuthenticator, skip filter")
		return false
	}
	if authenName == "" {
		klog.V(3).Infof("tenant filter: %s %s: without auth namespace or err: %v from groups/name(%s/%s)",
			reqInfo.Verb, req.URL.String(), tenant.GetErr(), strings.Join(groups, ","), name)
		return false
	}

	if !reqInfo.IsResourceRequest {
		return false
	}
	return t.Approver.Approve(reqInfo.Resource, reqInfo.Verb)

}

func (t *TenantLabel) Filter(req *http.Request) (err error) {
	var tenantName string
	ctx := req.Context()
	reqInfo, ok := kuberequest.RequestInfoFrom(ctx)
	if !ok {
		err = fmt.Errorf("nou found requestInfo in filter %s", FilterName)
		return
	}
	user, ok := kuberequest.UserFrom(ctx)
	if !ok {
		err = fmt.Errorf("not found user in filter %s", FilterName)
		return
	}
	groups := user.GetGroups()
	name := user.GetName()
	tenant, _ := request.TenantFrom(ctx)
	tenantName = tenant.GetName()

	s := filterutil.CreateSerializer(req, t.serializerManager)
	if s == nil {
		return fmt.Errorf("failed to create serializer in TenantLabel Filter")
	}

	body, err := LimitedReadBody(req, t.MaxRequestBodyBytes)
	if err != nil {

	}
	obj, err := s.Decode(body)
	if err != nil || obj == nil {
		return fmt.Errorf("failed to decode request in TenantLabel Filter, %v", err)
	}

	reqVal := reflect.ValueOf(obj)
	if reqVal.Kind() != reflect.Ptr {
		err = fmt.Errorf("Skip tenant isolation label: resource(%s) obj kind invalid by groups/user(%s/%s)",
			reqInfo.Resource, strings.Join(groups, ","), name)
		return
	}

	reqElem := reqVal.Elem()
	reqType := reqVal.Type()
	for i := 0; i < reqVal.NumField(); i++ {
		if reqType.Field(i).Name == "ObjectMeta" {
			metaField := reqElem.Field(i)
			metaObj, ok := metaField.Interface().(metav1.ObjectMeta)
			if !ok {
				err = fmt.Errorf("Skip tenant isolation label: resource(%s) metav1.ObjectMeta invalid by groups/user(%s/%s)",
					reqInfo.Resource, strings.Join(groups, ","), name)
				return
			}

			metaVal := reflect.ValueOf(&metaObj)
			if metaVal.Kind() != reflect.Ptr {
				err = fmt.Errorf("Skip tenant isolation label: resource(%s) obj kind invalid by groups/user(%s/%s)",
					reqInfo.Resource, strings.Join(groups, ","), name)
				return
			}

			metaElem := metaVal.Elem()
			metaType := metaVal.Type()
			for j := 0; j < metaVal.NumField(); j++ {
				if metaType.Field(j).Name == "Labels" {
					labelField := metaElem.Field(j)
					labelType := labelField.Type()

					if labelField.IsNil() {
						labelField.Set(reflect.MakeMap(labelType))
					}
					labels, ok := labelField.Interface().(map[string]string)
					if ok {
						if val, found := labels[util.TenantLabelKey]; !found {
							klog.V(2).Infof("tenant isolation label: resource(%s: %s) added label(%s) by groups/user(%s/%s)",
								reqInfo.Resource, reqInfo.Name, tenantName, strings.Join(groups, ","), name)
							labels[util.TenantLabelKey] = tenantName
						} else {
							if val != tenantName {
								err = fmt.Errorf("skip tenant isolation label: resource(%s) already with defined label(%s) by groups/user/ns(%s/%s/%s).",
									reqInfo.Resource, val, strings.Join(groups, ","), name, tenantName)
								return
							}
						}
					}
				}
			}
			reqElem.Field(i).Set(metaElem)
		}
	}
	return nil
}

func LimitedReadBody(req *http.Request, limit int64) ([]byte, error) {
	defer req.Body.Close()
	if limit <= 0 {
		return ioutil.ReadAll(req.Body)
	}
	lr := &io.LimitedReader{
		R: req.Body,
		N: limit + 1,
	}
	data, err := ioutil.ReadAll(lr)
	if err != nil {
		return nil, err
	}
	if lr.N <= 0 {
		return nil, errors.NewRequestEntityTooLargeError(fmt.Sprintf("limit is %d", limit))
	}
	return data, nil
}
