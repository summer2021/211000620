/*
Copyright 2020 The OpenYurt Authors.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package tenantfilter

import (
	"fmt"
	"net/http"
)

type filterChain []Interface

// Approve returns whether there is a filter in chain that can process the request.
func (fc filterChain) Approve(req *http.Request) bool {
	for _, f := range fc {
		if f.Approve(req) {
			return true
		}
	}
	return false
}

// Filter filters the request. The request will be processed in the order of filter registration, and the request
// can only be processed by one filter. On success, the request is returned. On failure, an error is returned. If
// the request is not processed by any filter, an error is returned.
func (fc filterChain) Filter(req *http.Request) (err error) {
	for _, f := range fc {
		if !f.Approve(req) {
			continue
		}
		return f.Filter(req)
	}
	return fmt.Errorf("filters can't process request and deny it")
}
