/*
Copyright 2020 The OpenYurt Authors.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package app

import (
	"fmt"
	"net/url"
	"time"

	"github.com/spf13/cobra"
	utilerrors "k8s.io/apimachinery/pkg/util/errors"
	"k8s.io/apiserver/pkg/util/term"
	"k8s.io/client-go/informers"
	"k8s.io/client-go/kubernetes"
	clientset "k8s.io/client-go/kubernetes"
	"k8s.io/client-go/rest"
	"k8s.io/client-go/tools/clientcmd"
	"k8s.io/client-go/util/keyutil"
	cliflag "k8s.io/component-base/cli/flag"
	"k8s.io/component-base/cli/globalflag"
	"k8s.io/klog/v2"
	"k8s.io/kubernetes/pkg/api/legacyscheme"
	kubeauthenticator "k8s.io/kubernetes/pkg/kubeapiserver/authenticator"
	"k8s.io/kubernetes/pkg/serviceaccount"
	utilflag "k8s.io/kubernetes/pkg/util/flag"

	"sidecar/cmd/yurt-tenant-sidecar/app/config"
	"sidecar/cmd/yurt-tenant-sidecar/app/options"
	"sidecar/pkg/projectinfo"
	"sidecar/pkg/sidecar/iptables"
)

// NewSideCarCommand creates a *cobra.Command object with default parameters
func NewSideCarCommand(stopCh <-chan struct{}) *cobra.Command {
	s := options.NewSideCarOptions()

	cmd := &cobra.Command{
		Use: "tenant-sidecar",
		Long: `The sidecar intercepts the requests sent to Kubernetes API
server, executes user authentication and request processing so that
tenants can only be visible to their resources.`,
		RunE: func(cmd *cobra.Command, args []string) error {
			if s.Version {
				fmt.Printf("%s: %#v\n", "tenant-sidecar", projectinfo.Get())
				return nil
			}
			fmt.Printf("%s version: %#v\n", "tenant-sidecar", projectinfo.Get())

			utilflag.PrintFlags(cmd.Flags())

			// set defalut options
			completedOptions, err := Complete(s)
			if err != nil {
				return err
			}

			// validate options
			if errs := completedOptions.Validate(); len(errs) != 0 {
				return utilerrors.NewAggregate(errs)
			}

			return Run(completedOptions, stopCh)
		},
	}

	fs := cmd.Flags()
	namedFlagSets := s.Flags()
	globalflag.AddGlobalFlags(namedFlagSets.FlagSet("global"), cmd.Name())
	for _, f := range namedFlagSets.FlagSets {
		fs.AddFlagSet(f)
	}

	cols, _, _ := term.TerminalSize(cmd.OutOrStdout())
	usageFmt := "Usage:\n  %s\n"
	cmd.SetUsageFunc(func(cmd *cobra.Command) error {
		fmt.Fprintf(cmd.OutOrStderr(), usageFmt, cmd.UseLine())
		cliflag.PrintSections(cmd.OutOrStderr(), namedFlagSets, cols)
		return nil
	})
	cmd.SetHelpFunc(func(cmd *cobra.Command, args []string) {
		fmt.Fprintf(cmd.OutOrStdout(), "%s\n\n"+usageFmt, cmd.Long, cmd.UseLine())
		cliflag.PrintSections(cmd.OutOrStdout(), namedFlagSets, cols)
	})

	return cmd
}

// Run runs the SideCarOptions. This should never exit.
func Run(completeOptions completedSideCarOptions, stopCh <-chan struct{}) error {
	sidecarConfig, err := createGenericConfig(completeOptions)
	if err != nil {
		return err
	}

	sidecarServer, err := sidecarConfig.Complete().New()
	if err != nil {
		return err
	}

	err = iptables.EnsureIptables(sidecarConfig.MasterPort, sidecarConfig.Port)
	if err != nil {
		return err
	}

	return sidecarServer.Run(stopCh)
}

// createGenericConfig takes the sidecar server options and produces the generic config associated with it.
func createGenericConfig(s completedSideCarOptions) (genericConfig *config.Config, lastErr error) {
	genericConfig = config.NewConfig()

	if lastErr = s.Generic.ApplyTo(genericConfig); lastErr != nil {
		return
	}

	u, err := url.Parse("localhost:" + s.MasterPort)
	if err != nil {
		lastErr = fmt.Errorf("failt to parse master address: %v", err)
	}
	genericConfig.MasterPort = s.MasterPort
	genericConfig.Master = u

	sharedInformer, err := createSharedInformers(fmt.Sprintf("http://%s", genericConfig.Master))
	if err != nil {
		lastErr = fmt.Errorf("failed to create shared informers: %v", err)
		return
	}
	genericConfig.SharedInformer = sharedInformer

	genericConfig.Client, err = clientset.NewForConfig(genericConfig.Kubeconfig)
	if err != nil {
		lastErr = fmt.Errorf("fail to generate client from kubeconfig: %v", err)
		return
	}

	if lastErr = s.Authentication.ApplyTo(
		&genericConfig.Authentication,
		genericConfig.SecureServing,
		genericConfig.Client,
		sharedInformer); lastErr != nil {
		return
	}

	err = s.TenantFilter.ApplyTo(genericConfig)
	if err != nil {
		lastErr = fmt.Errorf("failed to initialize filter: %v", err)
		return
	}
	return
}

// createSharedInformers create sharedInformers from the given proxyAddr.
func createSharedInformers(proxyAddr string) (informers.SharedInformerFactory, error) {
	var kubeConfig *rest.Config
	var err error
	kubeConfig, err = clientcmd.BuildConfigFromFlags(proxyAddr, "")
	if err != nil {
		return nil, err
	}

	client, err := kubernetes.NewForConfig(kubeConfig)
	if err != nil {
		return nil, err
	}

	return informers.NewSharedInformerFactory(client, 24*time.Hour), nil
}

// completedServerRunOptions is a private wrapper that enforces a call of Complete() before Run can be invoked.
type completedSideCarOptions struct {
	*options.SideCarOptions
}

// Complete set default SideCarOptions.
// Should be called after sidecar flags parsed.
func Complete(s *options.SideCarOptions) (completedSideCarOptions, error) {
	var options completedSideCarOptions
	// Use (ServiceAccountSigningKeyFile != "") as a proxy to the user enabling
	// TokenRequest functionality. This defaulting was convenient, but messed up
	// a lot of people when they rotated their serving cert with no idea it was
	// connected to their service account keys. We are taking this opportunity to
	// remove this problematic defaulting.
	if s.ServiceAccountSigningKeyFile == "" {
		// Default to the private server key for service account token signing
		if len(s.Authentication.ServiceAccounts.KeyFiles) == 0 && s.SecureServing.ServerCert.CertKey.KeyFile != "" {
			if kubeauthenticator.IsValidServiceAccountKeyFile(s.SecureServing.ServerCert.CertKey.KeyFile) {
				s.Authentication.ServiceAccounts.KeyFiles = []string{s.SecureServing.ServerCert.CertKey.KeyFile}
			} else {
				klog.Warning("No TLS key provided, service account token authentication disabled")
			}
		}
	}

	if s.ServiceAccountSigningKeyFile != "" && s.Authentication.ServiceAccounts.Issuer != "" {
		sk, err := keyutil.PrivateKeyFromFile(s.ServiceAccountSigningKeyFile)
		if err != nil {
			return options, fmt.Errorf("failed to parse service-account-issuer-key-file: %v", err)
		}
		if s.Authentication.ServiceAccounts.MaxExpiration != 0 {
			lowBound := time.Hour
			upBound := time.Duration(1<<32) * time.Second
			if s.Authentication.ServiceAccounts.MaxExpiration < lowBound ||
				s.Authentication.ServiceAccounts.MaxExpiration > upBound {
				return options, fmt.Errorf("the serviceaccount max expiration must be between 1 hour to 2^32 seconds")
			}
		}

		s.ServiceAccountIssuer, err = serviceaccount.JWTTokenGenerator(s.Authentication.ServiceAccounts.Issuer, sk)
		if err != nil {
			return options, fmt.Errorf("failed to build token generator: %v", err)
		}
		s.ServiceAccountTokenMaxExpiration = s.Authentication.ServiceAccounts.MaxExpiration
	}
	return options, nil
}
