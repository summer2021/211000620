/*
Copyright 2020 The OpenYurt Authors.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package options

import (
	"time"

	genericoptions "k8s.io/apiserver/pkg/server/options"
	cliflag "k8s.io/component-base/cli/flag"
	"k8s.io/kubernetes/pkg/serviceaccount"

	sidecaroptions "sidecar/pkg/sidecar/options"
)

// SideCarOptions is the main settings for the sidecar
type SideCarOptions struct {
	Version                          bool
	MasterPort                       string
	SecureServing                    *genericoptions.SecureServingOptionsWithLoopback
	ServiceAccountSigningKeyFile     string
	ServiceAccountIssuer             serviceaccount.TokenGenerator
	ServiceAccountTokenMaxExpiration time.Duration
	Kubeconfig                       string
	Generic                          *sidecaroptions.GenericSideCarOptions
	Authentication                   *sidecaroptions.BuiltInAuthenticationOptions
	TenantFilter                     *sidecaroptions.TenantFilterOptions
}

// NewSideCarOptions creates a new NewSideCarOptions with a default config.
func NewSideCarOptions() *SideCarOptions {
	o := &SideCarOptions{
		Generic:        sidecaroptions.NewDefaultGenericSideCarOptions(),
		Authentication: sidecaroptions.NewBuiltInAuthenticationOptions().WithAll(),
		TenantFilter:   sidecaroptions.NewTenantFilterOptions(),
	}
	return o
}

// Validate checks SideCarOptions and return a slice of found errs.
func (o *SideCarOptions) Validate() []error {
	var errs []error

	errs = append(errs, o.Generic.Validate()...)
	errs = append(errs, o.Authentication.Validate()...)
	errs = append(errs, o.TenantFilter.Validate()...)
	return errs
}

// Flags returns flags for a specific sidecar by section name
func (o *SideCarOptions) Flags() (fss cliflag.NamedFlagSets) {
	o.Generic.AddFlags(fss.FlagSet("generic"))
	o.Authentication.AddFlags(fss.FlagSet("authentication"))
	o.TenantFilter.AddFlags(fss.FlagSet("tenant filter"))

	fs := fss.FlagSet("misc")
	fs.BoolVar(&o.Version, "version", o.Version, "print the version information.")
	fs.StringVar(&o.MasterPort, "masterport", o.MasterPort, "The address of the Kubernetes API server (overrides any value in kubeconfig).")

	//fs.Int64Var(&o.MaxConnectionBytesPerSec, "max-connection-bytes-per-sec", o.MaxConnectionBytesPerSec, ""+
	//	"If non-zero, throttle each user connection to this number of bytes/sec. "+
	//	"Currently only applies to long-running requests.")
	fs.StringVar(&o.ServiceAccountSigningKeyFile, "service-account-signing-key-file", o.ServiceAccountSigningKeyFile, ""+
		"Path to the file that contains the current private key of the service account token issuer. The issuer will sign issued ID tokens with this private key.")

	return fss
}
