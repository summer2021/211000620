/*
Copyright 2020 The OpenYurt Authors.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package config

import (
	"net/http"
	"net/url"
	"time"

	"github.com/openyurtio/openyurt/pkg/yurthub/kubernetes/serializer"
	"k8s.io/apimachinery/pkg/util/sets"
	utilwaitgroup "k8s.io/apimachinery/pkg/util/waitgroup"
	genericapifilters "k8s.io/apiserver/pkg/endpoints/filters"
	apirequest "k8s.io/apiserver/pkg/endpoints/request"
	genericapiconfig "k8s.io/apiserver/pkg/server"
	genericapiserver "k8s.io/apiserver/pkg/server"
	genericfilters "k8s.io/apiserver/pkg/server/filters"
	"k8s.io/apiserver/pkg/server/healthz"
	"k8s.io/client-go/informers"
	clientset "k8s.io/client-go/kubernetes"
	restclient "k8s.io/client-go/rest"

	authenticator "sidecar/pkg/sidecar/authentication"
	sidecarserver "sidecar/pkg/sidecar/server"
	"sidecar/pkg/sidecar/tenantfilter"
	handlerutil "sidecar/pkg/sidecar/util/handler"
)

// Config is a structure used to configure a sidecar.
type Config struct {
	MasterPort string
	Master     *url.URL
	Address    string
	Port       string
	InsurePort string
	ServerPort string
	// BuildHandlerChainFunc allows you to build custom handler chains.
	BuildHandlerChainFunc func(apiHandler http.Handler, c *Config) (secure http.Handler)
	RequestInfoResolver   apirequest.RequestInfoResolver
	// SecureServing is required to serve https
	SecureServing     *genericapiconfig.SecureServingInfo
	SerializerManager *serializer.SerializerManager
	SharedInformer    informers.SharedInformerFactory
	// the rest config for the master
	Kubeconfig *restclient.Config
	// the general kube client
	Client *clientset.Clientset
	// Authentication is the configuration for authentication
	Authentication genericapiserver.AuthenticationInfo
	Filters        tenantfilter.Interface

	// HandlerChainWaitGroup allows you to wait for all chain handlers exit after the server shutdown.
	HandlerChainWaitGroup *utilwaitgroup.SafeWaitGroup
	// Predicate which is true for paths of long-running http requests
	LongRunningFunc apirequest.LongRunningRequestCheck
	// The default set of healthz checks. There might be more added via AddHealthChecks dynamically.
	HealthzChecks []healthz.HealthChecker
	// The default set of livez checks. There might be more added via AddHealthChecks dynamically.
	LivezChecks []healthz.HealthChecker
	// The default set of readyz-only checks. There might be more added via AddReadyzChecks dynamically.
	ReadyzChecks []healthz.HealthChecker
	// The limit on the request size that would be accepted and decoded in a write request
	// 0 means no limit.
	MaxRequestBodyBytes int64
	// MaxRequestsInFlight is the maximum number of parallel non-long-running requests. Every further
	// request has to wait. Applies only to non-mutating requests.
	MaxRequestsInFlight int
	// MaxMutatingRequestsInFlight is the maximum number of parallel mutating requests. Every further
	// request has to wait.
	MaxMutatingRequestsInFlight int
	// If specified, all requests except those which match the LongRunningFunc predicate will timeout
	// after this duration.
	RequestTimeout time.Duration
	// If specified, long running requests such as watch will be allocated a random timeout between this value, and
	// twice this value.  Note that it is up to the request handlers to ignore or honor this timeout. In seconds.
	MinRequestTimeout int
	// This represents the maximum amount of time it should take for sidecar to complete its startup
	// sequence and become healthy. From sidecar's start time to when this amount of time has
	// elapsed, /livez will assume that unfinished post-start hooks will complete successfully and
	// therefore return true.
	LivezGracePeriod time.Duration
	// ShutdownDelayDuration allows to block shutdown for some time, e.g. until endpoints pointing to sidecar
	// have converged on all node. During this time, the sidecar keeps serving, /healthz will return 200,
	// but /readyz will return failure.
	ShutdownDelayDuration time.Duration
}

// NewConfig returns a Config struct with the default values
func NewConfig() *Config {
	defaultHealthChecks := []healthz.HealthChecker{healthz.PingHealthz, healthz.LogHealthz}
	cfg := &genericapiserver.Config{
		LegacyAPIGroupPrefixes: sets.NewString(genericapiserver.DefaultLegacyAPIPrefix),
	}
	resolver := genericapiserver.NewRequestInfoResolver(cfg)

	return &Config{
		BuildHandlerChainFunc: DefaultBuildHandlerChain,
		HandlerChainWaitGroup: new(utilwaitgroup.SafeWaitGroup),
		RequestInfoResolver:   resolver,
		SerializerManager:     serializer.NewSerializerManager(),

		HealthzChecks:               append([]healthz.HealthChecker{}, defaultHealthChecks...),
		ReadyzChecks:                append([]healthz.HealthChecker{}, defaultHealthChecks...),
		LivezChecks:                 append([]healthz.HealthChecker{}, defaultHealthChecks...),
		MaxRequestsInFlight:         400,
		MaxMutatingRequestsInFlight: 200,
		RequestTimeout:              time.Duration(60) * time.Second,
		MinRequestTimeout:           1800,
		LivezGracePeriod:            time.Duration(0),
		ShutdownDelayDuration:       time.Duration(0),
		MaxRequestBodyBytes:         int64(3 * 1024 * 1024),
		// Default to treating watch as a long-running operation
		// Generic API servers have no inherent long-running subresources
		LongRunningFunc: genericfilters.BasicLongRunningRequestCheck(sets.NewString("watch"), sets.NewString()),
	}
}

type completedConfig struct {
	*Config
}

// CompletedConfig same as Config, just to swap private object.
type CompletedConfig struct {
	// Embed a private pointer that cannot be instantiated outside of this package.
	*completedConfig
}

// Complete fills in any fields not set that are required to have valid data. It's mutating the receiver.
func (c *Config) Complete() *CompletedConfig {
	cc := completedConfig{c}
	if c.MasterPort == "" {
		c.MasterPort = "6443"
	}
	return &CompletedConfig{&cc}
}

func DefaultBuildHandlerChain(handler http.Handler, c *Config) http.Handler {
	handler = handlerutil.TrackCompleted(handler)
	handler = tenantfilter.WithTenantFilter(handler, c.Filters)
	handler = handlerutil.TrackStarted(handler, "tenantfilter")

	handler = handlerutil.TrackCompleted(handler)
	handler = authenticator.WithAuthentication(handler, c.Authentication.Authenticator)
	handler = handlerutil.TrackStarted(handler, "authentication")

	handler = genericfilters.WithTimeoutForNonLongRunningRequests(handler, c.LongRunningFunc, c.RequestTimeout)
	handler = genericfilters.WithWaitGroup(handler, c.LongRunningFunc, c.HandlerChainWaitGroup)
	handler = genericapifilters.WithRequestInfo(handler, c.RequestInfoResolver)

	return handler
}

// New creates a new server which logically combines the handling chain with the passed server.
func (c CompletedConfig) New() (sidecarserver.Server, error) {
	handlerChainBuilder := func(handler http.Handler) http.Handler {
		return c.BuildHandlerChainFunc(handler, c.Config)
	}
	sidecarHandler := sidecarserver.NewSideCarReverseProxyHandler(c.Master, handlerChainBuilder)

	s, err := sidecarserver.NewSideCarServer(sidecarHandler, c)
	return s, err
}
