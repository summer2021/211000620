# 边缘资源独占，云端管控共享的非侵入多租隔离能力

## 背景介绍
1. 大量的IOT用户的边缘资源规模比较小，为每个用户创建一个独立的边缘托管集群(如阿里云的ACK@Edge)，虽然可以满足用户需求，但是成本偏高。
2. 同时K8s原生多租能力弱，仅提供简单的namespace维度资源隔离

因此我们考虑在边缘托管集群(标准K8s集群)中提供多租隔离的能力。

## 项目目标
边缘资源是用户所有的，因此边缘资源是用户独占的，也意味物理资源是隔离的。因此从边缘侧看多租隔离能力，主要是考虑边缘节点上业务的多租隔离能力。表现为租户A的业务或者节点上组件发起的请求，只能获取到租户A相关的数据。如下图所示:

![image-20210719112802063](./images/edge-tenant-isolation.png)


## 功能需求
边缘发起的请求主要有如下两类：
- 请求类型1:  业务Pod发起的请求，一般是基于ServiceAccount做认证鉴权的。ServiceAccount包含的认证鉴权信息可以参考: https://kubernetes.io/docs/reference/access-authn-authz/authentication/#service-account-tokens
- 请求类型2:  组件(如kubelet)发起的请求，一般是基于节点证书来做认证鉴权的。节点证书包含的认证鉴权信息可以参考：https://kubernetes.io/docs/reference/access-authn-authz/authentication/#x509-client-certs, https://kubernetes.io/docs/reference/access-authn-authz/certificate-signing-requests/

从目标我们可以分析到，主要是保证边缘发起的请求只能获取到对应租户的数据。因此主要有如下功能需求：
1. 拦截边缘发起的请求(包括Pod和组件发起的请求)，解析请求中包含的认证信息，在请求中增加租户相关的过滤信息
2. 针对绕过拦截的边缘请求，在云端直接拒绝掉非法请求

## 期待成果
1. 详细的设计文档
2. 可运行的功能实现代码
3. 相关测试代码
